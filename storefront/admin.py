from django.contrib import admin
from storefront.models import Storefront


# Register your models here.



class StorefrontAdmin(admin.ModelAdmin):
    pass


admin.site.register(Storefront, StorefrontAdmin)