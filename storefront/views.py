from email.policy import default
from django.shortcuts import render, redirect
from storefront.models import Storefront
from django.contrib.auth.mixins import LoginRequiredMixin
from django.views.generic.list import ListView
from django.views.generic.edit import CreateView
from django.views.generic.detail import DetailView

# Create your views here.


class StorefrontListView(LoginRequiredMixin, ListView):
    models = Storefront
    template_name = "storefront/list.html"
    paginate_by = 10

    def get_queryset(self):
        return Storefront.objects.filter(members=self.request.user)

class StorefrontDetailView(LoginRequiredMixin, DetailView):
    models = Storefront
    template_name = "storefront/detail.html"
    
    def get_queryset(self):
        return Storefront.objects.filter(members=self.request.user)


class StorefrontCreateView(LoginRequiredMixin, CreateView):
    models = Storefront
    template_name = "storefront/create.html"
    fields = ["name", "description" ]

    def form_valid(self, form):
        owner = form.save(commit=False)
        owner.user = self.request.user
        owner.save()
        return redirect("store_detailview", pk=owner.id)