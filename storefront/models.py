from django.db import models
from django.conf import settings

# Create your models here.


USER_MODEL = settings.AUTH_USER_MODEL

class Storefront(models.Model):
    name = models.CharField(max_length=125)
    description = models.TextField()
    created = models.DateTimeField(auto_now_add=True)
    image = models.URLField(null=True, blank=True)
    members = models.ManyToManyField(USER_MODEL, related_name="storefront")

    def __str__(self):
        return self.name