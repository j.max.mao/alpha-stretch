from django.urls import path
from storefront.views import(
    StorefrontCreateView,
    StorefrontListView,
    StorefrontDetailView,
)



urlpatterns = [
    path("", StorefrontListView.as_view(), name="store_listview"),
    path("create/", StorefrontCreateView.as_view(), name="store_createview"),
    path("detail/", StorefrontDetailView.as_view(), name="store_detailview")
]